let cartItems = [];

export function addToCart(productId,productName, price, ...quantities) {

    quantities.forEach(quantity => {
        const parsedQuantity = parseInt(quantity, 10);
        const existingItem = cartItems.find(item => item.id === productId);
        if (existingItem) {
            existingItem.quantity += parsedQuantity;
        } else {
            cartItems.push({ id:productId,productName, price, quantity: parsedQuantity });
        }
    });
    renderCart();
}

export function removeFromCart(id) {
    cartItems = cartItems.filter(item => item.id !== id);
    renderCart();
}

export function clearCart() {
    cartItems = [];
    renderCart();
}

export function getCartItems() {
    return cartItems;
}
// ...

export function increaseQuantity(index) {
    const cartItems = getCartItems();
    cartItems[index].quantity++;
    renderCart();
}

export function decreaseQuantity(index) {
    const cartItems = getCartItems();
    if (cartItems[index].quantity > 1) {
        cartItems[index].quantity--;
        renderCart();
    }
}
