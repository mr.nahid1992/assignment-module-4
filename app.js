import {products} from './product.js';
import {addToCart, removeFromCart, clearCart, getCartItems, increaseQuantity, decreaseQuantity} from './cart.js';

function renderCart() {
    const cartItems = getCartItems();

    const cartTableBody = document.getElementById("cartTableBody");
    const productCountElement = document.getElementById("productCount");
    cartTableBody.innerHTML = "";

    let totalPrice = 0;
    let totalCount = 0;

    cartItems.map((item ,index) => {
        const row = document.createElement("tr");
        row.innerHTML = `
      <td>${index + 1}</td>
      <td>${item.productName}</td>
      <td>
        <button class="btn btn-sm btn-danger" onclick="decreaseQuantity(${index})">-</button>
        <span>${item.quantity}</span>
        <button class="btn btn-sm btn-success" onclick="increaseQuantity(${index})">+</button>
      </td>
      <td>$${item.price}</td>
      <td>$${item.quantity * item.price}</td>
      <td><button class="btn btn-sm btn-danger" onclick="removeFromCart('${item.id}')">Remove</button></td>
    `;

        cartTableBody.appendChild(row);
        totalPrice += item.price * item.quantity;
        totalCount += item.quantity;
    });


    // Add total row
    const totalRow = document.createElement("tr");
    totalRow.innerHTML = `
    <td colspan="4" class="font-semibold">Total</td>
    <td>$${totalPrice}</td>
    <td></td>
  `;

    cartTableBody.appendChild(totalRow);

    productCountElement.textContent = totalCount;
}


window.addToCart = addToCart;
window.removeFromCart = removeFromCart;
window.clearCart = clearCart;
window.renderCart = renderCart;
window.increaseQuantity = increaseQuantity;
window.decreaseQuantity = decreaseQuantity;


const initializeProducts = () => {
    const productContainer = document.getElementById("productContainer");
    const productCards = products.map((product) => {
        const productCard = document.createElement("div");
        productCard.classList.add("card", "h-full", "hover:shadow-lg");
        productCard.innerHTML = `
            <div class="p-4">
                <img src="${product.image}" alt="${product.name}" class="w-full h-36 object-cover rounded-t-lg">
            </div>
            <div class="card-body">
                <h5 class="card-title">${product.name}</h5>
                <p class="card-text">$${product.price}</p>
                <div class="flex items-center">
                    <label for="quantity_${product.id}" class="mr-2">Quantity:</label>
                    <select id="quantity_${product.id}" class="form-select border border-gray-300 rounded px-2 py-1">
                        ${generateQuantityOptions()}
                    </select>
                </div>
               <button class="btn btn-primary mt-2" onclick="addToCart('${product.id}','${product.name}', ${product.price}, document.getElementById('quantity_${product.id}').value)">Add to Cart</button>
            </div>
        `;

        return productCard;
    });

    productContainer.append(...productCards);
};

const generateQuantityOptions = () => {
    let options = "";
    for (let i = 1; i <= 10; i++) {
        options += `<option value="${i}">${i}</option>`;
    }
    return options;
};



document.addEventListener('DOMContentLoaded', () => {
    initializeProducts();
});
