export const products = [
    { id:1,name: 'Haier 1.5 Ton Non-Inverter TurboCool AC - HSU-18TurboCool: (PA)', price: 32900 ,image: "img/product_1.webp"},
    { id:2,name: 'Haier 32 HD Ready Miracast LED TV (H32D2M)', price: 15900 ,image: "img/product_2.webp"},
    { id:3,name: 'Haier 32 Android 11 HD Smart LED TV (H32K66GH) with Free Bongo Subscription', price: 20900 ,image: "img/product_3.webp"},
    { id:4,name: 'Haier H55K66UG 55  Android 11.0 Smart 4k Led TV', price: 57900 ,image: "img/product_4.webp"},
    { id:5,name: 'Haier Smart Tv Remote-Black - Netflix Subscription', price: 745 ,image: "img/product_5.webp"},
    { id:6,name: 'Haier 319l Magic Cool No Frost Refrigerator (HRF-330WDBG)', price: 57318 ,image: "img/product_6.webp"},
    { id:7,name: 'Haier 301L Chest Freezer (HCF-340)', price: 45232 ,image: "img/product_7.webp"},
    { id:8,name: 'Haier 1.5 Ton Non-Inverter TurboCool AC - HSU-18TurboCool: (PA)', price: 56990 ,image: "img/product_8.webp"},
    { id:9,name: 'Product 9', price: 25 ,image: "default-img.png"},
];
